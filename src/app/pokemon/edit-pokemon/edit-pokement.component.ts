import { Component, OnInit } from '@angular/core';
import { Pokemon } from '../pokemon';
import { ActivatedRoute } from '@angular/router';
import { PokemonService } from '../pokemon.service';

@Component({
  selector: 'app-edit-pokement',
  template: `
   <h2 class="center">
      Editer {{ pokemon?.name }}
   </h2>
   <p *ngIf="pokemon" class="center">
    <img [src]="pokemon.picture">
    </p>
    <!-- Utilisation du composant 'app-pokemon-form' pour afficher le formulaire d'édition du pokémon -->
    <app-pokemon-form *ngIf="pokemon" [pokemon]="pokemon"></app-pokemon-form>
  `,
  styles: ``
})
export class EditPokementComponent implements OnInit {

  /**
   * Le pokémon à éditer.
   */
  pokemon: Pokemon | undefined;

  constructor(
    private route: ActivatedRoute,
    private pokemonService: PokemonService
  ) {}

  /**
   * Méthode du cycle de vie appelée après que le composant a été initialisé.
   */
  ngOnInit() {
    // Récupération de l'identifiant du pokémon depuis les paramètres de l'URL
    const pokemonId: string | null = this.route.snapshot.paramMap.get('id');
    // Vérification si un identifiant de pokémon est présent dans l'URL
    if (pokemonId) {
      // Appel de la méthode getPokemonById du service PokemonService pour récupérer le pokémon correspondant à l'identifiant
      this.pokemonService.getPokemonById(+pokemonId).subscribe(pokemon => {
        // Assignation du pokémon récupéré à la propriété pokemon du composant
        this.pokemon = pokemon;
      });
    }
  }
}
