import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Pokemon } from '../pokemon';
import { PokemonService } from '../pokemon.service';

@Component({
  selector: 'app-detail-pokemon',
  templateUrl: './detail-pokemon.component.html',
  styles: ``
})
export class DetailPokemonComponent implements OnInit{

  pokemon: Pokemon | undefined;

  constructor(
    private route: ActivatedRoute, 
    private router: Router,
    private pokemonService: PokemonService
  ) {}

  ngOnInit() {
    // Récupération de l'identifiant du pokémon depuis les paramètres de l'URL
    const pokemonId: string | null = this.route.snapshot.paramMap.get('id');
  
    // Vérification si un identifiant de pokémon est présent dans l'URL
    if (pokemonId) {
      // Appel de la méthode getPokemonById du service PokemonService pour récupérer le pokémon correspondant à l'identifiant
      this.pokemonService.getPokemonById(+pokemonId).subscribe(pokemon => {
        // Assignation du pokémon récupéré à la propriété pokemon du composant
        this.pokemon = pokemon;
      });
    }
  }
  

  // Méthode pour naviguer vers la liste des pokémons
  goToPokemonList() {
    this.router.navigate(['/pokemons']);
  }

  // Méthode pour naviguer vers la page d'édition d'un pokémon spécifique
  goToEditPokemon(pokemon: Pokemon) {
    this.router.navigate(['/edit/pokemon/', pokemon.id]);
  }
}
