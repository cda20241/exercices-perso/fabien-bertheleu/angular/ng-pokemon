import { Directive, ElementRef, HostListener, Input } from '@angular/core';

@Directive({
  selector: '[pkmBorderCard]'
})
export class BorderCardDirective {

  @Input('pkmBorderCard') borderColor: string; // Alias pour la couleur de la bordure

  // Couleurs et styles par défaut
  private fontColorInitial: string = '#e0bc1b';
  private fontColorOver: string = '#1227a0';
  private borderColorInitial: string = '#e0bc1b';
  private borderColorOver: string = '#1227a0';
  private radiusInitial: number = 10;
  private radiusOver: number = 50;
  private backgroundInitial: string = '#4682B4';

  constructor(private el: ElementRef) {
    // Initialisation des styles
    this.setHeight(180);
    this.setBorder(this.borderColorInitial);
    this.setRadius(this.radiusInitial);
    this.setBackground(this.backgroundInitial);
    this.setFontColor(this.fontColorInitial);
    this.setShadow('10px 10px 5px rgba(0,0,0,0.5)');
  }

  // Événement lors du survol de la souris
  @HostListener('mouseenter') onMouseEnter() {
    this.setBorder(this.borderColor || this.borderColorOver); 
    this.setBackground('#e0bc1b');
    this.setFontColor(this.fontColorOver);
    this.setFontWeight('bold');
    this.setRadius(this.radiusOver);
    this.setTransition('0.3s');
    this.setShadow('10px 10px 5px rgba(0,0,0,0.9)');
  }

  // Événement lorsque la souris quitte la zone
  @HostListener('mouseleave') onMouseLeave() {
    this.setHeight(180);
    this.setBorder(this.borderColorInitial);
    this.setRadius(this.radiusInitial);
    this.setBackground(this.backgroundInitial);
    this.setFontColor(this.fontColorInitial);
    this.setFontWeight('normal');
    this.setTransition('0.3s');
    this.setShadow('10px 10px 5px rgba(0,0,0,0.5)');
  }

  // Définition de la hauteur de l'élément
  setHeight(height: number) {
    this.el.nativeElement.style.height = `${height}px`;
  }

  // Définition de la bordure
  setBorder(color: string) {
    this.el.nativeElement.style.border = `solid 4px ${color}`;
  }

  // Définition du rayon de la bordure
  setRadius(radius: number) {
    this.el.nativeElement.style.borderRadius = `${radius}px`;
  }

  // Définition de la couleur de fond
  setBackground(color: string) {
    this.el.nativeElement.style.background = `${color}`;
  }

  // Définition de la couleur de la police
  setFontColor(color: string) {
    this.el.nativeElement.style.color = `${color}`;
  }

  // Définition du poids de la police
  setFontWeight(weight: string) {
    this.el.nativeElement.style.fontWeight = `${weight}`;
  }

  // Définition de la transition
  setTransition(transition: string) {
    this.el.nativeElement.style.transition = `${transition}`;
  }

  // Définition de l'ombre
  setShadow(shadow: string) {
    this.el.nativeElement.style.boxShadow = `${shadow}`;
  }
}
