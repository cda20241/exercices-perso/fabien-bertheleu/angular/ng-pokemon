import { Component, Input, OnInit } from '@angular/core';
import { PokemonService } from '../pokemon.service';
import { Router } from '@angular/router';
import { HttpResponse } from '@angular/common/http';
import { Pokemon } from '../pokemon';
import e from 'express';

@Component({
  selector: 'app-pokemon-form',
  templateUrl: './pokemon-form.component.html',  // Template associé au composant
  styleUrls: [`./pokemon-form.component.css`], // Styles associés au composant
})
export class PokemonFormComponent implements OnInit {
  @Input() pokemon: Pokemon; // Entrée de Pokémon provenant d'un composant parent

  types: string[]|undefined;  // Liste des types de Pokémon

  constructor(
    private pokemonService: PokemonService, // Service pour récupérer les données des Pokémon
    private router: Router
  ) { }

  ngOnInit(): void { // Méthode exécutée lors de l'initialisation du composant
    this.types = this.pokemonService.getPokemonTypeList(); // Initialisation de la liste des types
  }

  // Vérifie si le Pokémon a un type spécifié
  hasType(type: string): boolean {
    return this.pokemon.types.includes(type); // Vérifie si le type est inclus dans les types du Pokémon
  }

  isTypesValid(type: string): boolean {
    if(this.pokemon.types.length === 1 && this.hasType(type)) { // Si le pokemons a un seul type et qu'il a le type en paramètre  
      return false;      
    }
    if(this.pokemon.types.length > 2 && !this.hasType(type)) { // Si le pokemons a plus de deux types et qu'il n'a pas le type en paramètre
      return false;
    }
    return true;  
  }

  selectType($event: Event, type: string): void {
    const isChecked: boolean = ($event.target as HTMLInputElement).checked; // Vérifie si le type est cocheé ou non
    if(isChecked) { // Ajoute le type au pokemons si il est cocheé
      this.pokemon.types.push(type);
    } else { // Retire le type du pokemons si il n'est pas cocheé
      const index = this.pokemon.types.indexOf(type);
      if(index >= 0) {
        this.pokemon.types.splice(index, 1);
      }
    }
  }

  onSubmit() {
    console.log('Submit form !');
    this.router.navigate(['/pokemon', this.pokemon.id]);
    // Soumet le formulaire
  } 
}
