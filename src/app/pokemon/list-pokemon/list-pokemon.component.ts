import { Component } from '@angular/core';
import { Router } from '@angular/router';

import { Pokemon } from '../pokemon';
import { PokemonService } from '../pokemon.service';
import { OnInit } from '@angular/core';

@Component({
  selector: 'app-list-pokemon',
  templateUrl: './list-pokemon.component.html',
})
export class ListPokemonComponent implements OnInit {

  /**
   * Liste des Pokémon à afficher.
   */
  pokemonList: Pokemon[] ;
  
  /**
   * Pokémon sélectionné.
   */
  pokemonSelected: Pokemon | undefined;

  constructor(
    private router: Router,
    private pokemonService: PokemonService) { }

  /**
   * Initialisation du composant.
   */
  ngOnInit() {
    // subscribe me permet de m'abonner à l'obervable "this.pokemonService.getPokemonList()" et de recevoir les valeurs renvoyées par l'API.
    // je vais recevoir une pokemonList et avec cette pokemon List, je vais l'attribuer à la propriété pokemonList
    this.pokemonService.getPokemonList().subscribe(pokemonList => this.pokemonList = pokemonList);
  }

  /**
   * Navigue vers la page détaillée d'un Pokémon.
   * @param pokemon Le Pokémon sélectionné.
   */
  goToPokemon(pokemon: Pokemon) {
    this.pokemonSelected = pokemon;
    this.router.navigate(['/pokemon', pokemon.id]);
  }
}
