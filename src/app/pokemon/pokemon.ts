/**
 * Représente un Pokémon avec ses attributs.
 */
export class Pokemon {
    id: number;
    hp: number;
    cp: number;
    name: string;
    picture: string;
    types: Array<string>;
    created: Date;
}