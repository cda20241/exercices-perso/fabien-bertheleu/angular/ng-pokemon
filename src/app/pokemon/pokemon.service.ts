import { Injectable } from '@angular/core';
import { Pokemon } from './pokemon';
import { HttpClient } from '@angular/common/http';
import { response } from 'express';
import { of } from 'rxjs';
import { catchError, tap, Observable  } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class PokemonService {

  constructor(private http: HttpClient) { }


  /**
   * Récupère la liste des pokémons.
   * @returns un tableau de pokémons
   */
  getPokemonList(): Observable<Pokemon[]> {
    // Effectue une requête HTTP GET pour récupérer la liste des pokémons depuis l'API
    return this.http.get<Pokemon[]>('api/pokemons').pipe(
      // Effectue un effet de côté pour afficher la liste des pokémons dans la console
      // tap permet d'exécuter une action pour chaque élément émis par l'observable, sans modifier ces éléments.
      tap((pokemonList) => console.table(pokemonList)),
      // Gère les erreurs potentielles de la requête HTTP
      catchError((error) => {
        // Affiche l'erreur dans la console
        console.log(error);
        // Retourne un observable contenant un tableau vide en cas d'erreur
        return of([]);
      })
    );
  } 
  // return POKEMONS;   =>> quand on travaillait sans l'API
  


/**
   * Récupère un Pokémon par son identifiant depuis l'API.
   * @param pokemonId - L'identifiant du Pokémon à récupérer
   * @returns un observable contenant le Pokémon correspondant à l'identifiant donné, ou undefined en cas d'erreur
   */
getPokemonById(pokemonId: number): Observable<Pokemon | undefined> {
  // Effectue une requête HTTP GET pour récupérer le Pokémon avec l'identifiant donné depuis l'API
  return this.http.get<Pokemon>(`api/pokemons/${pokemonId}`).pipe(      
    // Effectue un effet de côté (tap) pour afficher le Pokémon dans la console
    tap((response) => console.log('getPokemonById', response)),         
    // Gère les erreurs potentielles de la requête HTTP
    catchError((error) => this.handleError(error, []))
  );
}

/**
 * Affiche les données reçues dans la console sous forme de tableau.
 * @param response - Les données à afficher
 */
private log(response: Pokemon[] | Pokemon | undefined) {
  console.table(response);
}

/**
 * Gère les erreurs en affichant l'erreur dans la console et en retournant une valeur par défaut.
 * @param error - L'erreur à gérer
 * @param errorValue - La valeur par défaut à retourner en cas d'erreur
 * @returns un observable contenant la valeur par défaut
 */
private handleError(error: Error, errorValue: any): Observable<any> {
  // Affiche l'erreur dans la console
  console.log(error);
  // Retourne un observable contenant la valeur par défaut
  return of(errorValue);
}
 /**
  /**
   * Récupère la liste des types de pokémons disponibles.
   * @returns un tableau contenant les différents types de pokémons
   */
  getPokemonTypeList(): string[]|undefined {
    return [
      "Plante",
      "Feu",
      "Eau",
      "Insecte",
      "Normal",
      "Electrik",
      "Poison",
      "Fée",
      "Vol",
      "Psy"
    ];
  }
  
}
