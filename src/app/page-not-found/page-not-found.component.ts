import { Component } from '@angular/core';

@Component({
  selector: 'app-page-not-found',
  template: `
    <!-- Contenu de la page 404 -->
    <div class="center">
      <!-- Image illustrative -->
      <img src="http://assets.pokemon.com/assets/cms2/img/pokedex/full/035.png" alt="">

      <!-- Message d'erreur -->
      <h1>Hey, cette page n'existe pas !</h1>

      <!-- Bouton de retour -->
      <a routerLink="/pokemons" class="waves-effect waves-teal btn-flat">
        Retourner sur la liste
      </a>
    </div>
  `,
  styles: ``
})
export class PageNotFoundComponent {
  /**
   * Composant pour afficher une page indiquant que la page demandée n'a pas été trouvée.
   */
}
