import { Component, OnInit } from '@angular/core';
import { POKEMONS } from './pokemon/mock-pokemon-list';
import { Pokemon } from './pokemon/pokemon';

@Component({
  selector: 'app-root',
  /**
   * Définit le template HTML de la composante principale de l'application.
   */
  templateUrl: 'app.component.html'
})  
export class AppComponent  {}
