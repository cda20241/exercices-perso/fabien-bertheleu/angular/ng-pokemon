import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { PageNotFoundComponent } from './page-not-found/page-not-found.component';

const routes: Routes = [
    /**
     * Redirige vers la route '/pokemons' si l'URL est vide ou contient 'pokemon'.
     */
    { path: '' || 'pokemon', redirectTo: 'pokemons', pathMatch: 'full' },
    /**
     * Redirige vers le composant 'PageNotFoundComponent' si aucune correspondance de route n'est trouvée.
     */
    { path: '**', component: PageNotFoundComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
