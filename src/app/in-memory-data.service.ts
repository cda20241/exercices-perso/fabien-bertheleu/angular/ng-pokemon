import { Injectable } from '@angular/core';
import { InMemoryDbService,  } from 'angular-in-memory-web-api';
import { POKEMONS } from './pokemon/mock-pokemon-list';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class InMemoryDataService implements InMemoryDbService {

  /**
   * Crée une base de données fictive pour stocker les données des pokémons.
   * @returns un objet contenant les données des pokémons
   */
  createDb(){
    const pokemons = POKEMONS;    
    return { pokemons };
  }
    
}
